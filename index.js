let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

function usersArray(newUser) {
    // let newUser = prompt("Please input new User")
    users[users.length] = newUser;
    console.log("This is the new users Array:"); 
    console.log(users)
}

usersArray("John Cena"); // invoked "John Cena" to be placed at the last part of users Array

/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/
let itemFound; // function that will access the users[] array
function indexReceiver([x]){
    
    return itemFound = users[x]; // places the value of user[x] in itemFound variable
}
indexReceiver([2]); // this is the number of index we want to access in users
console.log("This the value of itemFound:");
console.log(itemFound);


/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/

function deleteLastItem() {
    let x = users.length - 1; // targets the last index of users Array
    let lastItemInArray = users[x]; // function scoped variable that will store the last item in users Array
    users.length = x; // removing last item in users Array
    console.log(lastItemInArray);
    console.log("John Cena will do his special move!")
    // console.log(users);
    return lastItemInArray; // returning the last item in the array which is stored in the variable
    // console.log(lastItemInArray);
}
deleteLastItem();
console.log("John Cena Disappeared in thin AIR!!!")
console.log(users)
/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

function updateUserArray(update, indexNumber) {
    users[indexNumber] = update; // accessing the specified index in users Array and replacing it with the update
}

updateUserArray("The Big Show", 1); // This is invoking to replace the second item in the users Array and replace with "The Big Show"
console.log("Steve Austin left the group.");
console.log("BUT HERE COMES THE BIG SHOW!")
console.log(users);


/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.
        -Invoke the function.
        -Outside of the function, Log the users array in the console.


*/

function modifyLengthOfArray(newLength){
    users.length = newLength; // this will modify the length of the users Array to the specified newLength
}
let newLeng = 0;
modifyLengthOfArray(newLeng);
if(newLeng > 0){
    console.log("Now it's time for a one on one!")
} else{
    console.log("Wanna see a magic trick?")
}
console.log(users);


/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/
let isUsersEmpty;

function arrayChecker(usersLength){ //if statement that will show if isUserEmpty is true or false
    if(usersLength > 0) {
        return isUsersEmpty = false;
    } else {
        return isUsersEmpty = true;
    }
}
arrayChecker(users.length)
console.log("Is users Array empty?");
console.log(isUsersEmpty);